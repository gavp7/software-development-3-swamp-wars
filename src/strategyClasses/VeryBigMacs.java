package strategyClasses;

public class VeryBigMacs implements Diet {

	public String diet() {
		return "very big macs";
	}

	public void announceDietChange() {
		System.out.println("The Ogres Diet is now Very Big Macs!");		
	}

}
