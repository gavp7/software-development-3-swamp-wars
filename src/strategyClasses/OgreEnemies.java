package strategyClasses;

public class OgreEnemies implements Diet {

	public String diet() {
		return "ogre enemies";
	}

	public void announceDietChange() {
		System.out.println("The ogres diet is now Ogre Enemies!");		
	}

}
