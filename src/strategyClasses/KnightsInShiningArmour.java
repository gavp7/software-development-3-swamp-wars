package strategyClasses;

public class KnightsInShiningArmour implements Diet {
	
	public String diet() {
		return "knights in shining armour";
	}

	public void announceDietChange() {
		System.out.println("The Ogres diet is now Knights In Shining Armour!");		
	}

}
