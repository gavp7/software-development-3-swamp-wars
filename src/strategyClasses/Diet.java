package strategyClasses;

import java.util.ArrayList;

public interface Diet {
public String diet();
public void announceDietChange();
}
