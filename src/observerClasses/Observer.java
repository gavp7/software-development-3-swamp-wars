package observerClasses;


public interface Observer {
public void update(int x, int y);
}
