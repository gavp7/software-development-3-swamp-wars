package observerClasses;

import characters.Enemy;

public interface Observable {
	public void registerObserver(Enemy o);
	public void removeObserver(Enemy o);
	public void notifyObservers();
}
