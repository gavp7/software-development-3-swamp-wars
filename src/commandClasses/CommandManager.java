package commandClasses;

import java.util.Stack;

public class CommandManager {

	public CommandManager(){}

	private Stack commandStack = new Stack();

	public void executeCommand(Command c) {
		c.execute();
		commandStack.push(c);


	}
public boolean isUndoAvailable()
{
	return !commandStack.empty();
}

	public void undo() {
		if(isUndoAvailable())
		{
			Command c = (Command) commandStack.pop();
			c.undo();
		}
	}
}
