package commandClasses;

import characters.MoveablePlayer;

public class EnemyCommand implements Command {
	MoveablePlayer enemy;
	int previousEnemyX;
	int previousEnemyY;
	public EnemyCommand(MoveablePlayer m)
	{
		enemy = m;
		previousEnemyX =enemy.getX();
		previousEnemyY = enemy.getY();
	}
	public void execute() {
		enemy.Move();
	}

	public void undo() {
		enemy.setX(previousEnemyX);
		enemy.setY(previousEnemyY);

	}

}
