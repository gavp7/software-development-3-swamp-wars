package commandClasses;

import java.util.ArrayList;
import java.util.HashSet;

import characters.MoveablePlayer;
import characters.Ogre;
import factoryClasses.CharacterCreation;

public class ExecuteCommand implements Command {
	CharacterCreation cc;
	Ogre ogre;


	HashSet<MoveablePlayer> mp;
	int previousOgreX;
	int previousOgreY;
	public ExecuteCommand(CharacterCreation c)
	{

		ogre = Ogre.getOgre();
		cc = c;
		previousOgreX =ogre.getX();
		previousOgreY = ogre.getY();


	}
	
	public void execute() {
		ogre.Move();				
		ogre.eat();			
		mp = cc.removeDead();												
		cc.CreateEnemy();	

	}

	public void undo() {
		ogre.setX(previousOgreX);
		ogre.setY(previousOgreY);
		for(MoveablePlayer m : mp){
			cc.getEnemies().add(m);
		}
	}



}
