package factoryClasses;
import java.util.Random;

import characters.Donkey;
import characters.MoveablePlayer;
import characters.Parrot;
import characters.Snake;




public class Factory {
	public void randomEnemy(){

		Random r = new Random();
		int Low = 0;
		int High = 2;
		int R = r.nextInt(High-Low) + Low;
		createPlayer(R);
	}

	public MoveablePlayer createPlayer(int type) {
		MoveablePlayer mp = null;
		if (type == 1) {
			mp = new Parrot();
		} else if (type == 2) {
			mp = new Donkey();
		} else if (type == 3) {
			mp = new Snake();
		}

		return mp;

	}
}
