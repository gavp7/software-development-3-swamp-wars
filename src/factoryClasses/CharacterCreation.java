package factoryClasses;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;

import characters.Enemy;
import characters.MoveablePlayer;
import characters.Ogre;



public class CharacterCreation {
	
	ArrayList<MoveablePlayer> enemies = new ArrayList<MoveablePlayer>();
	Factory factory;

	public CharacterCreation(){
		setTheFactory(new Factory());
	}

	public HashSet<MoveablePlayer> removeDead(){
		HashSet<MoveablePlayer> deadEnemies = new HashSet<MoveablePlayer>();
		Ogre ogre = Ogre.getOgre();
		for(MoveablePlayer mp : enemies){
			if(mp.isDead()){ //if an enemy is dead, add them to the dead enemies list
				deadEnemies.add(mp);
			}
		}
		for(MoveablePlayer mp : deadEnemies){ //remove dead enemies from lists
			ogre.removeObserver((Enemy) mp);
			enemies.remove(mp);
			mp.announceCharacterDeath();
		}
		return deadEnemies;
	}

	public ArrayList<MoveablePlayer> getEnemies() {
		return enemies;
	}

	public void setEnemies(ArrayList<MoveablePlayer> enemies) {
		this.enemies = enemies;
	}

	public void CreateEnemy(){
		Random rand = new Random();
		int Lowr = 0;
		int Highr = 3;
		int create = rand.nextInt(Highr-Lowr) + Lowr;
		if(create == 2){
			MoveablePlayer e = new MoveablePlayer();
			Random r = new Random();
			int Low = 1;
			int High = 4;
			int R = r.nextInt(High-Low) + Low;
			e =this.factory.createPlayer(R);
			e.announceCharacter();
			enemies.add(e);
		}
	}
	public void setTheFactory(Factory em) {
		this.factory = em;
	}
}
