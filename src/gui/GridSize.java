package gui;

//Used to define the size of the grid used in the game
public class GridSize {
private static int x;
private static int y;
public static int getX() {
	return x;
}
public static void setX(int _x) {
	x = _x;
}
public static int getY() {
	return y;
}
public static void setY(int _y) {
	y = _y;
}
}
