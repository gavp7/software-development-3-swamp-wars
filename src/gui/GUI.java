package gui;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;

import commandClasses.CommandManager;
import commandClasses.EnemyCommand;
import commandClasses.ExecuteCommand;
import strategyClasses.KnightsInShiningArmour;
import strategyClasses.OgreEnemies;
import strategyClasses.VeryBigMacs;
import characters.Enemy;
import characters.MoveablePlayer;
import characters.Ogre;
import factoryClasses.CharacterCreation;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import java.awt.Font;

public class GUI extends JFrame {

	private JPanel contentPane;



	public GUI() {
		setTitle("Swamp Wars");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 110, 832, 687);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);

		final JPanel panel = new JPanel(new GridLayout(GridSize.getX(), GridSize.getY()));
		final JPanel labels[][] = new JPanel[GridSize.getX()][GridSize.getY()];
		final ImageIcon ogreIcon = new ImageIcon("ogre.jpg");
		final ImageIcon donkeyIcon = new ImageIcon("donkey.jpg");
		final ImageIcon parrotIcon = new ImageIcon("parrot.png");
		final ImageIcon snakeIcon = new ImageIcon("snake.png");

		final JLabel ogrepic = new JLabel(ogreIcon);
		//Creates a JPanel for each square in grid
		for (int i =  0; i < GridSize.getX(); i++)
		{
			for(int j = 0; j < GridSize.getY(); j++)
			{
				labels[i][j] = new JPanel();
				labels[i][j].setBorder(BorderFactory.createLineBorder(Color.black));

				panel.add(labels[i][j]);
			}
		}
		final CommandManager cm = new CommandManager();
		final Ogre ogre = Ogre.getOgre();		
		final JLabel lbldiet = new JLabel("New label");

		lbldiet.setText(ogre.getDiet().diet());

		//Places ogre picture on grid
		labels[ogre.getY()][ogre.getX()].add(ogrepic);								
		final CharacterCreation cc = new CharacterCreation();	
		//buttons to conduct actions
		JButton btnNextTurn = new JButton("Next Turn");				
		btnNextTurn.addActionListener(new ActionListener() {						
			public void actionPerformed(ActionEvent arg0) {		
				//remove ogre picture from grid
				labels[ogre.getY()][ogre.getX()].removeAll();	
				labels[ogre.getY()][ogre.getX()].repaint();
				//remove enemy pictures from grid
				for(MoveablePlayer mp : cc.getEnemies()){	
					labels[mp.getY()][mp.getX()].removeAll();	
					labels[mp.getY()][mp.getX()].repaint();
				}
				//execute all commands and store them in commandmanager
				for(MoveablePlayer mp : cc.getEnemies()){	
					cm.executeCommand(new EnemyCommand(mp));
				}
				cm.executeCommand(new ExecuteCommand(cc));

				//redraw all enemies and ogre depending on enemy type
				for(MoveablePlayer mp : cc.getEnemies()){	
					Enemy e = (Enemy)mp;
					if((e.getEnemy() == "snake"))
					{
						final JLabel snakepic = new JLabel(snakeIcon);
						labels[mp.getY()][mp.getX()].add(snakepic);
						labels[mp.getY()][mp.getX()].revalidate();
					}
					if((e.getEnemy() == "donkey"))
					{
						final JLabel donkeypic = new JLabel(donkeyIcon);
						labels[mp.getY()][mp.getX()].add(donkeypic);
						labels[mp.getY()][mp.getX()].revalidate();
					}
					if((e.getEnemy() == "parrot"))
					{
						final JLabel parrotpic = new JLabel(parrotIcon);
						labels[mp.getY()][mp.getX()].add(parrotpic);
						labels[mp.getY()][mp.getX()].revalidate();
					}
				}	

				System.out.println("\n");											

				final JLabel ogrepic = new JLabel(ogreIcon);

				labels[ogre.getY()][ogre.getX()].add(ogrepic);
				labels[ogre.getY()][ogre.getX()].revalidate();


			}																		


		});


		//these buttons dynamically change the ogres diet 
		JButton btnVeryBigMacs = new JButton("Very Big Macs");
		btnVeryBigMacs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Ogre o = Ogre.getOgre();
				o.setDiet(new VeryBigMacs());
				o.DietType();
				lbldiet.setText(ogre.getDiet().diet());


			}
		});

		JButton btnKnightsInShining = new JButton("Knights In Shining Armour");
		btnKnightsInShining.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Ogre o = Ogre.getOgre();
				o.setDiet(new KnightsInShiningArmour());
				o.DietType();
				lbldiet.setText(ogre.getDiet().diet());

			}
		});

		JButton btnOgreEnemies = new JButton("Ogre Enemies");
		btnOgreEnemies.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Ogre o = Ogre.getOgre();
				o.setDiet(new OgreEnemies());
				o.DietType();
				lbldiet.setText(ogre.getDiet().diet());

			}
		});

		JLabel lblDiet = new JLabel("Diet:");
		lblDiet.setFont(new Font("Tahoma", Font.PLAIN, 15));
		JLabel lblCurrentDiet = new JLabel("Current Diet:");

		JButton btnUndo_1 = new JButton("Undo");
		btnUndo_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//this class is very similar to the 'next turn' button, however it will undo all the changes to the previous state
				labels[ogre.getY()][ogre.getX()].removeAll();	
				labels[ogre.getY()][ogre.getX()].repaint();

				for(MoveablePlayer mp : cc.getEnemies()){	
					labels[mp.getY()][mp.getX()].removeAll();	
					labels[mp.getY()][mp.getX()].repaint();
				}
				MoveablePlayer toBeRemoved = null;
				cm.undo();
				for(MoveablePlayer mp : cc.getEnemies()){	
					cm.undo();
				}

				for(MoveablePlayer mp : cc.getEnemies()){
					if(mp.getX() == 0 && mp.getY() == 0)
					{
						if(!cm.isUndoAvailable())
						{
							toBeRemoved = mp;
						}
					}
				}
				cc.getEnemies().remove(toBeRemoved);
				final JLabel ogrepic = new JLabel(ogreIcon);

				labels[ogre.getY()][ogre.getX()].add(ogrepic);
				labels[ogre.getY()][ogre.getX()].revalidate();
				for(MoveablePlayer mp : cc.getEnemies()){	
					Enemy e2 = (Enemy)mp;
					if((e2.getEnemy() == "snake"))
					{
						final JLabel snakepic = new JLabel(snakeIcon);
						labels[mp.getY()][mp.getX()].add(snakepic);
						labels[mp.getY()][mp.getX()].revalidate();
					}
					if((e2.getEnemy() == "donkey"))
					{
						final JLabel donkeypic = new JLabel(donkeyIcon);
						labels[mp.getY()][mp.getX()].add(donkeypic);
						labels[mp.getY()][mp.getX()].revalidate();
					}
					if((e2.getEnemy() == "parrot"))
					{
						final JLabel parrotpic = new JLabel(parrotIcon);
						labels[mp.getY()][mp.getX()].add(parrotpic);
						labels[mp.getY()][mp.getX()].revalidate();
					}
				}	
			}
		});
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\gavin\\Documents\\Swamp Wars\\home.jpg"));





		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(23)
							.addComponent(lblDiet, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnKnightsInShining))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnOgreEnemies))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnVeryBigMacs))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblCurrentDiet))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(lbldiet))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 196, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnUndo_1))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnNextTurn)))
					.addGap(29)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 544, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(30)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(lblDiet, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnKnightsInShining)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnOgreEnemies)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnVeryBigMacs)
							.addGap(42)
							.addComponent(lblCurrentDiet)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lbldiet)
							.addGap(28)
							.addComponent(lblNewLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addGap(18)
							.addComponent(btnUndo_1)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnNextTurn)
							.addGap(43))
						.addComponent(panel, GroupLayout.DEFAULT_SIZE, 583, Short.MAX_VALUE))
					.addGap(24))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
