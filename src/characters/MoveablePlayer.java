package characters;

import gui.GridSize;

import java.util.Random;

import commandClasses.Command;
//class is used to deal with the player movement and determining if the character is dead
public class MoveablePlayer implements Command{
	private int x;
	private int y;
	private boolean dead = false;
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public boolean isDead() {
		return dead;
	}

	public void setDead(boolean dead) {
		this.dead = dead;
	}
	public void announceCharacter() {
	}
	public void announceCharacterDeath(){
	}
	public void Move(){
		//Generates a random number and moves the player depending on that number
		int direction = GenerateRandom(0, 8);

		if(direction == 0){
			setX(x - 1);
			//if x < 0, the player is off the grid and so will be moved in the opposite direction
			if (getX() < 0){
				setX(x+2);
			}
		} else if(direction == 1){
			setX(getX()+1);

			//if x > GridSize, the player is off the grid and will be moved in the opposite direction
			if(getX() > GridSize.getX() - 1){
				setX(getX() - 2);
			}
			
		}else if(direction == 2 ){
			setY(y - 1);

			if(getY() < 0){
				setY(getY()+2);
			}
			
		} else if(direction == 3){
			setY(y + 1);

			if(getY() > GridSize.getY() - 1){
				setY(getY()-2);
			}
		} else if(direction == 4){
			setX(x-1);
			setY(y-1);

			if(getX() < 0 && getY() < 0){
				setX(getX()+2);
				setY(getY()+2);
			}
			if (getX() < 0){
				setX(getX()+2);	
			}
			if(getY() < 0){
				setY(getY()+2);
			}
			} else if(direction ==5){
				setX(x+1);
				setY(y+1);
				if(getY() > GridSize.getY() -1 && getX() > GridSize.getX() - 1){
					setX(getX() -2);
					setY(getY()- 2);
				}
				if(getY() > GridSize.getY() - 1){
					setY(getY() - 2);
				}
				if(getX() > GridSize.getX() - 1){
					setX(getX() - 2);
				}
				
			} else if (direction == 6){
				setX(x+1);
				setY(y-1);
				if(getX() > GridSize.getX() - 1 && getY() < 0){
					setX(getX() - 2);
					setY(getY() + 2);
				}
				if(getX() > GridSize.getX() - 1){
					setX(getX() - 2);
				}
				if(getY() < 0){
					setY(getY() + 2);
				}
				
			} else if(direction == 7){
				setX(x-1);
				setY(y+1);

				if(getY() > GridSize.getY() - 1 && getX() < 0){
					setY(getY() - 2);
					setX(getX() + 2);
				}
				if(getY() > GridSize.getY() - 1){
					setY(getY() - 2);
				}
				if(getX() < 0){
					setX(getX() + 2);
				}
			}
		announceMove();
		}
		
	public void announceMove(){
		
	}
	//generates a random number 
	public int GenerateRandom(int low, int high){
		Random r = new Random();
		int Low = low;
		int High = high;
		int x = r.nextInt(High-Low) + Low;
		return x;
	}
	public void execute() {
		// TODO Auto-generated method stub
		
	}
	public void undo() {
		// TODO Auto-generated method stub
		
	}
}
