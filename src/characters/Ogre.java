package characters;

import gui.GridSize;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import observerClasses.Observable;
import strategyClasses.Diet;
import strategyClasses.KnightsInShiningArmour;

public class Ogre extends MoveablePlayer implements Observable{

	public static Ogre ogre;
	private ArrayList<Enemy> myEnemies = new ArrayList<Enemy>();
	private Diet diet;

	public Ogre(){
		//randomly places ogre in grid
		int x = GenerateRandom(0, GridSize.getX());
		int y = GenerateRandom(0, GridSize.getY());
		setX(x);
		setY(y);
		//default diet is 'knights in shining armour'
		setDiet(new KnightsInShiningArmour());
		setDead(false);

	}

	public static Ogre getOgre(){
		if (ogre == null){
			ogre = new Ogre();
		}
		return ogre;
	}

	public Diet getDiet() {
		return diet;
	}
	public ArrayList<Enemy> getEnemies(){
		return myEnemies;
	}

	public void setDiet(Diet diet) {
		this.diet = diet;
	}
	public void DietType(){
		this.diet.announceDietChange();
	}
	public void announceMove()
	{
		System.out.println("The Ogre has moved to Position " + getX() +  "," + getY());
		notifyObservers();
	}

	public void announceCharacter()
	{
		System.out.println("The Ogre has Appeared! at Position " + getX() + "," + getY());
	}
	public void announceCharacterDeath(){
		System.out.println("The Ogre is dead");
	}
	public void registerObserver(Enemy o) {
		this.myEnemies.add(o);		
	}
	public void removeObserver(Enemy o) {
		this.myEnemies.remove(o);		
	}
	public void notifyObservers() {
		for(Enemy tempEnemy: this.myEnemies){
			tempEnemy.update(getX(), getY());		

		}
	}
	//all of the enemies who are on the same position as the ogre are dealt with
	//if there is more than one enemy then the ogre diet has to be 'ogre enemies
	public void eat(){

		ArrayList<Enemy> battleEnemies = new ArrayList<Enemy>();

		for(Enemy e : myEnemies){
			if(e.getX() == this.getX() && e.getY() ==getY()){
				battleEnemies.add(e);
			}
		}
		if(battleEnemies.size() == 1){
			for(Enemy e: battleEnemies){
				e.setDead(true);
			}
		}
		if(battleEnemies.size() >1 ){
			if(ogre.getDiet().diet().equals("ogre enemies")){
				if(battleEnemies.size() >=3)
				{
					ogre.setDead(true);
				}
				else
				{
					for(Enemy e: battleEnemies){
						e.setDead(true);
					}
				}
			} else{
				ogre.setDead(true);
				System.out.println("The Ogre has died");
				JOptionPane.showMessageDialog(null, "The Ogre has Died.");
				System.exit(0);
			}
		}
	}
}



