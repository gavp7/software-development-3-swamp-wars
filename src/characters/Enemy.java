package characters;


import observerClasses.Observer;


public abstract class Enemy extends MoveablePlayer implements Observer{

	protected String enemy;
	private Ogre ogre; //The ogre whos position is observed 
	private int ogrex; //ogres coordinates
	private int ogrey;
	public Enemy(){

		setX(0); //every enemy spawns in the top left corner of the map
		setY(0);
		Ogre ogre = Ogre.getOgre();
		setOgre(ogre);
	}
	public String getEnemy() {
		return enemy;
	}

	public void setEnemy(String enemy) {
		this.enemy = enemy;
	}

	public void announceCharacter(){
		System.out.println("An ogre hating " + this.enemy + " has appeared!");
	}
	public void announceCharacterDeath(){
		System.out.println("The Ogre ate the " + this.enemy + "!");
	}


	public void announceMove(){
		System.out.println(this.enemy + " has moved to Position " + getX() + "," + getY());
	}

	private void setOgre(Ogre ogre)
	{
		this.ogre = ogre;
		this.ogre.registerObserver(this);
	}
	//Updates ogre position for enemy class
	public void update(int x, int y) {
		this.ogrex = x;
		this.ogrey = y;
	}
}
